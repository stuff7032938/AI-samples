-- asset loading

local Assets = {}

function Assets:load()
  self.Tile = self.Tile or love.graphics.newImage("assets/tile.png")
  self.Cross = self.Cross or love.graphics.newImage("assets/cross.png")
  self.Circle = self.Circle or love.graphics.newImage("assets/circle.png")

  return self
end

return Assets