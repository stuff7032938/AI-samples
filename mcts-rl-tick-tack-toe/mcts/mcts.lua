-- generic monte carlo tree search

local MCTS = {}

MCTS.nodePrototype = -- functions with 'node' parameters expect at least this content
{
   parent = nil -- node
  ,children = nil -- array of nodes
}

function MCTS.new()
  local mcts = {}

  mcts.calculateNodeValue = nil -- function used to calculate node value in the mcts.select stage. Signature: (node) -> value
  mcts.nodeValueComparator = nil -- function used to select best value calculated via mcts.calculateNodeValue. Signature: (previous, current) -> bool
  mcts.nodeCanBeExpanded = nil -- function to test if node can have derivatives (if it`s not terminal, etc.), called in the mcts.select and mcts.expand stage. Signature: (node) -> bool
  mcts.expandNode = nil -- function used to generate child node in a given node, plus returns it (will be used in mcts.simulate), called in the mcts.expand stage. Signature: (node) -> newChildNode
  mcts.playoutFromNode = nil -- function is basically an implementation of the mcts.simulate stage. Signature: (node) -> any result
  mcts.propagateResultToNode = nil -- function used in the mcts.backpropagate to propagate result of the mcts.simulate stage (mcts.playoutFromNode return value). Signature: (node, result) -> void
  mcts.chooseChildNodeToUse = nil -- function used after all mcts stages to choose the 'answer'. Signature: (node) -> node


  function mcts:step(node)
    -- selection
    local selectedNode = self:select(node)

    -- expansion
    local nodeToSimulate = self:expand(selectedNode)

    -- simulation
    local simulationResult = self:simulate(nodeToSimulate)

    -- backpropagation
    self:backpropagate(nodeToSimulate, simulationResult)

    -- 'answer'
    return self.chooseChildNodeToUse(node)
  end


  function mcts:select(node)
    local nodeChildren = node.children
    if #nodeChildren == 0 or self.nodeCanBeExpanded(node) then
      return node
    end

    local bestChildNode = nil
    local bestChildNodeValue = 0.0

    for _, childNode in pairs(nodeChildren) do
      local childNodeValue = self.calculateNodeValue(childNode)

      if not bestChildNode or self.nodeValueComparator(bestChildNodeValue, childNodeValue) then
        bestChildNode = childNode
        bestChildNodeValue = childNodeValue
      end
    end

    return self:select(bestChildNode)
  end

  function mcts:expand(node)
    if self.nodeCanBeExpanded(node) then
      return self.expandNode(node)
    else
      return node
    end
  end

  function mcts:simulate(node)
    return self.playoutFromNode(node)
  end

  function mcts:backpropagate(node, result)
    self.propagateResult(node, result)

    local parentNode = node.parent
    if parentNode then
      return self:backpropagate(parentNode, result)
    end
  end

  return mcts
end

return MCTS