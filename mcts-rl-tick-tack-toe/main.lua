-- callbacks setup

local TTT = require "ttt/ttt"

love.load = TTT.init
love.update = TTT.tick
love.draw = TTT.render
love.mousepressed = TTT.input