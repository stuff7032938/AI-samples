-- config and debug setup

if arg[#arg] == "-debug" then
  require("mobdebug").start()
end

function love.conf(t)
    t.window.width = 450
    t.window.height = 450
end