-- game board stuff

local Board =
{
   Assets = nil -- requires Cross, Circle and Tile images

  ,Cross = 1
  ,Circle = 2
  ,Empty = 3

  ,Draw = -1
  ,InProgress = 0
}

function Board.new(inHumanPlayer, inFirstPlayer)
  inHumanPlayer = inHumanPlayer or Board.Circle
  inFirstPlayer = inFirstPlayer or inHumanPlayer

  local board =
  {
     tiles = -- initial board is empty
     {
        { Board.Empty, Board.Empty, Board.Empty }
       ,{ Board.Empty, Board.Empty, Board.Empty }
       ,{ Board.Empty, Board.Empty, Board.Empty }
     }

    ,humanPlayer = inHumanPlayer
    ,aiPlayer = Board.getOpponent(inHumanPlayer)

    ,currentPlayer = inFirstPlayer
    ,player = Board.getOpponent(inFirstPlayer)

    ,lastMove = { x = -1, y = -1 }
  }

  function board:copy()
    local boardCopy = Board.new(
       self.humanPlayer
      ,self.currentPlayer
    )

    for x=1, 3 do
      for y=1, 3 do
        boardCopy:setTile(x, y, self:getTile(x, y))
      end
    end

    boardCopy.lastMove =
    {
      x = self.lastMove.x,
      y = self.lastMove.y
    }

    -- board copy shouldn't be rendered, even by accident
    boardCopy.draw = function() end

    return boardCopy
  end

  function board:setTile(x, y, mark)
    self.tiles[y][x] = mark
  end

  function board:getTile(x, y)
    return self.tiles[y][x]
  end

  function board:getTilesRow(rowNumber)
    local tilesRow = {}

    for i=1, 3 do
      table.insert(tilesRow, self.tiles[rowNumber][i])
    end

    return tilesRow
  end

  function board:getTilesColumn(columnNumber)
    local tilesColumn = {}

    for j=1, 3 do
      table.insert(tilesColumn, self.tiles[j][columnNumber])
    end

    return tilesColumn
  end

  function board:getTilesMainDiagonale()
    local mainDiagonale = {}

    for i=1, 3 do
      table.insert(mainDiagonale, self:getTile(i, i))
    end

    return mainDiagonale
  end

  function board:getTilesSecondaryDiagonale()
    local secondaryDiagonale = {}

    for i=1, 3 do
      table.insert(secondaryDiagonale, self:getTile(i, 4 - i))
    end

    return secondaryDiagonale
  end

  function board:tileIsEmpty(x, y)
    return self:getTile(x, y) == Board.Empty
  end

  function board:makeMove(x, y, player)
    self:setTile(x, y, player or self.currentPlayer)

    self.lastMove = { x = x, y = y }
  end

  function board:tryMakeMove(x, y, player)
    if not self:tileIsEmpty(x, y) then
      return false
    end
    
    self:makeMove(x, y, player)

    return true
  end
  
  function board:isAITurn()
    return self.currentPlayer == self.aiPlayer
  end

  function board:isHumanTurn()
    return self.currentPlayer == self.humanPlayer
  end

  function board:getOpponent()
    return Board.getOpponent(self.currentPlayer)
  end

  function board:switchPlayer()
    self.player = self.currentPlayer
    self.currentPlayer = self:getOpponent()
  end

  function board:getPossibleMoves()
    local possibleMoves = {}

    for x=1, 3 do
      for y=1, 3 do
        if self:tileIsEmpty(x, y) then
          local tile = {
            x = x,
            y = y
          }

          table.insert(possibleMoves, tile)
        end
      end
    end

    return possibleMoves
  end

  function board:isGameEnd()
    return self:getStatus() ~= Board.InProgress
  end

  function board:getStatus()
    local status

    for i=1, 3 do
      status = Board.checkStatus( self:getTilesRow(i) )

      if status ~= Board.Draw then
        return status
      end

      for j=1, 3 do
        status = Board.checkStatus( self:getTilesColumn(j) )

        if status ~= Board.Draw then
          return status
        end

      end
    end

    status = Board.checkStatus( self:getTilesMainDiagonale() )
    if status ~= Board.Draw then
      return status
    end

    status = Board.checkStatus( self:getTilesSecondaryDiagonale() )
    if status ~= Board.Draw then
      return status
    end

    local possibleMoves = self:getPossibleMoves()
    if #possibleMoves > 0 then
      status = Board.InProgress
    else
      status = Board.Draw
    end

    return status
  end

  function board:draw()
    for x=1, 3 do
      for y=1, 3 do
        local tile = self:getTile(x, y)

        local u = (x - 1) * 150
        local v = (y - 1) * 150

        love.graphics.draw(Board.Assets.Tile, u, v)
        if tile == Board.Cross then
          love.graphics.draw(Board.Assets.Cross, u, v)
        elseif tile == Board.Circle then
          love.graphics.draw(Board.Assets.Circle, u, v)
        end
      end
    end
  end

  if not Board.Assets then
    print "PC: Warning! Board.Assets is nil, Board:draw will not work!"

    board.draw = function() end
  end

  return board
end

function Board.constantToString(constant)
  if constant == Board.Cross then
    return "Cross"
  elseif constant == Board.Circle then
    return "Circle"
  elseif constant == Board.Draw then
    return "Draw"
  elseif constant == Board.InProgress then
    return "In Progress"
  elseif constant == Board.Empty then
    return "Empty"
  end
end

function Board.getOpponent(player)
  return 3 - player
end

function Board.checkStatus(tiles)
  local previousTile = nil

  for _, tile in pairs(tiles) do
    if tile == Board.Empty then
      return Board.Draw
    elseif previousTile == nil then
      previousTile = tile
    elseif previousTile ~= tile then
      return Board.Draw
    end
  end

  return previousTile
end

return Board