-- main game setup and flow logic

local Task = require "task/task"

local Board = require "ttt/board"

local Node = require "tree/node"

local MCTS = require "mcts/mcts"
local UCT = require "uct/uct"


local TTT = {}

function TTT.copyBoardStateForMCTS(state)
  local boardState
  if state then
    boardState = state:copy()
  else
    boardState = TTT.board:copy()
  end

  boardState.value = 0
  boardState.visits = 0

  function boardState:addValue(value)
    self.value = self.value + value
  end

  function boardState:incrementVisits()
    self.visits = self.visits + 1
  end

  return boardState
end

function TTT.getAllowedMovesForNode(node)
  local possibleMoves = node.state:getPossibleMoves()

  for _, childNode in pairs(node.children) do
    local moveTaken = childNode.state.lastMove

    for i, possibleMove in pairs(possibleMoves) do
      if ( possibleMove.x == moveTaken.x and
           possibleMove.y == moveTaken.y )
      then
        table.remove(possibleMoves, i) break
      end
    end
  end

  return possibleMoves
end


function TTT.handleAI(dt)
  if TTT.aiIterationsCounter == 0 then
    TTT.tree = Node.new( TTT.copyBoardStateForMCTS() )
  end

  local aiIterationsTimePassed = 0.0
  local aiIterationsTimeAllowed = love.timer.getAverageDelta()

  while aiIterationsTimePassed < aiIterationsTimeAllowed do
    if TTT.aiIterationsCounter < TTT.aiIterationsAllowed then
      local aiIterationTimeStart = love.timer.getTime()

      TTT.mcts:step(TTT.tree)

      local aiIterationTimeEnd = love.timer.getTime()
      local aiIterationTime = aiIterationTimeEnd - aiIterationTimeStart
      aiIterationsTimePassed = aiIterationsTimePassed + aiIterationTime

      TTT.aiIterationsCounter = TTT.aiIterationsCounter + 1
    else
      local node = TTT.mcts:step(TTT.tree)

      TTT.aiIterationsCounter = 0

      if node then
        local move = node.state.lastMove

        print(string.format(
          "AI: Placing mark on [%d, %d]", move.x, move.y
        ))

        TTT.makeMove( move.x, move.y )
      end

      break
    end
  end
end

function TTT.handleHuman(x, y)
  if TTT.tryMakeMove(x, y) then
    print(string.format(
      "Human: Placed mark on [%d, %d]", x, y
    ))
  else
    print "AI: That tile is not empty, try another one."
  end
end

function TTT.makeMove(x, y)
  TTT.board:makeMove(x, y)

  TTT.endTurn()
end

function TTT.tryMakeMove(x, y)
  local moveSucceeded = TTT.board:tryMakeMove(x, y)
  
  if moveSucceeded then
    TTT.endTurn()
  end

  return moveSucceeded
end

function TTT.endTurn()
  TTT.board:switchPlayer()

  if TTT.board:isGameEnd() then
    TTT.gameOver()
  end
end

function TTT.gameOver()
  print(string.format(
    "PC: Gameover! Result: %s",
    Board.constantToString( TTT.board:getStatus() )
  ))

  TTT.resetGame()
end

function TTT.initGame()
  print "PC: Starting new game!"

  TTT.board = Board.new(Board.Cross, Board.Circle)

  TTT.ready = true
end

function TTT.resetGame()
  TTT.ready = false

  local initGameTask = Task.newScheduledTask(
    2,
    function()
      TTT.initGame()
    end
  )

  TTT.taskManager:schedule(initGameTask)
end


local Assets = require "assets/assets"

function TTT.init()
  math.randomseed(os.time())

  -- scheduled events helper manager
  TTT.taskManager = Task.newManager()
  --

  -- asset preparation
  Board.Assets = Assets:load()
  --

  -- game setup
  TTT.initGame()

  TTT.aiIterationsAllowed = 15000
  TTT.aiIterationsCounter = 0
  --

  -- MCTS setup
  TTT.mcts = MCTS.new()

  TTT.uct = UCT.new()

  TTT.mcts.calculateNodeValue = function(node)
    return TTT.uct.calculate(
       node.state.value
      ,node.state.visits
      ,node.parent.state.visits
    )
  end

  TTT.mcts.nodeValueComparator = function(previous, current)
    return current > previous
  end

  TTT.mcts.nodeCanBeExpanded = function(node)
    local nodeChildren = node.children
    local possibleMoves = node.state:getPossibleMoves()

    return not node.state:isGameEnd() and #nodeChildren < #possibleMoves
  end

  TTT.mcts.expandNode = function(node)
    local allowedMoves = TTT.getAllowedMovesForNode(node)
    local allowedMove = allowedMoves[ math.random(#allowedMoves) ]

    local derivedState = TTT.copyBoardStateForMCTS(node.state)

    derivedState:makeMove( allowedMove.x, allowedMove.y )
    derivedState:switchPlayer()

    return node:addNewChild(derivedState)
  end

  TTT.mcts.playoutFromNode = function(node)
    local state = node.state:copy()

    while not state:isGameEnd() do
      local possibleMoves = state:getPossibleMoves()

      local randomMove = possibleMoves[ math.random(#possibleMoves) ]

      state:makeMove( randomMove.x, randomMove.y )
      state:switchPlayer()
    end

    return state:getStatus()
  end

  TTT.mcts.propagateResult = function(node, result)
    node.state:incrementVisits()

    local value = 0
    if result == node.state.player then
      value = 1
    elseif result ~= Board.Draw then
      value = -1
    end

    node.state:addValue(value)
  end

  TTT.mcts.chooseChildNodeToUse = function(node)
    local nodeToUse = nil
    local mostVisits = 0

    for _, childNode in pairs(node.children) do
      local nodeVisits = childNode.state.visits

      if not nodeToUse or nodeVisits > mostVisits then
        nodeToUse = childNode
        mostVisits = nodeVisits
      end
    end

    return nodeToUse
  end
  --
end

function TTT.tick(dt)
  if TTT.board:isAITurn() then
    TTT.handleAI(dt)
  end

  TTT.taskManager:process()
end

function TTT.render()
  TTT.board:draw()
end

function TTT.input(x, y)
  if not TTT.ready then return
  elseif TTT.board:isHumanTurn() then
    -- convert coordinates from screen to board
    x = math.ceil((x+1)/150)
    y = math.ceil((y+1)/150)

    TTT.handleHuman(x, y)
  elseif TTT.board:isAITurn() then
    print "AI: It`s my turn! Let me think a bit..."
  end
end

return TTT