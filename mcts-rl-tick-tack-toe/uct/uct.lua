-- upper confidence bound formula for a tree

local UCT = {}

UCT.explorationParam = math.sqrt(2) -- configurable bias

function UCT.new(inExplorationParam)
  local uct = {}

  uct.explorationParam = inExplorationParam or UCT.explorationParam -- another param can be passed
  
  function uct.calculate(value, visits, parentVisits)
    local exploitation = value -- node win/loss ratio, for example
    local exploration = uct.explorationParam * math.sqrt(math.log(parentVisits) / visits) -- high for moves with few simulations

    return exploitation + exploration
  end

  return uct
end

return UCT