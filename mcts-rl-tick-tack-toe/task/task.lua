-- scheduled task

local Task = {}

function Task.newScheduledTask(fireAfterSeconds, eventFunction)
  local task =
  {
     scheduledTime = love.timer.getTime() + fireAfterSeconds
    ,event = eventFunction
  }

  return task
end

function Task.newManager()
  local manager =
  {
    tasks = {}
  }

  function manager:schedule(task)
    table.insert(self.tasks, task)
  end

  function manager:process()
    local tasks = self.tasks
    local tasksNumber = #tasks
    
    local counter = tasksNumber
    while counter > 0 do
      local task = tasks[counter]

      if love.timer.getTime() > task.scheduledTime then
        task.event()

        table.remove(self.tasks, counter)
      end

      counter = counter - 1
    end
  end

  return manager
end

return Task