-- generic tree/node with custom data inside

local Node = {}

function Node.new(inData, inParent)
  local node =
  {
     parent = inParent
    ,children = {}
    ,state = inData -- arbitrary data can be used
  }

  function node:addNewChild(data)
    local newChildNode = Node.new(data, self)

    table.insert(self.children, newChildNode)
    return newChildNode
  end

  return node
end

return Node