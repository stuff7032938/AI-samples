# N-grams, plus 'rock, paper, scissors' game example

import random


# Simple N-gram implementation
class Ngram:
    def __init__(self, N: int, possible_events: list, max_events: int):
        assert N > 0
        assert max_events > 0
        assert N <= max_events
        assert len(possible_events) > 0

        self.N = N
        self.max_events = max_events
        self.possible_events = possible_events

    def predict_event(self):

        # Not enough events, return random possible event
        if len(self.events) < self.N:
            return random.choice(self.possible_events)
        elif self.N == 1:
            # Special case if this is a unigram
            # Just select event occured the most times
            event, = max(self.patterns, key=self.patterns.get)
            return event

        # Get 'window' for prediction
        window = tuple(self.events[-(self.N-1):])

        # Which patterns` N-1 events match window?
        window_matches = [i for i in self.patterns.keys() if i[0:self.N-1] == window]

        # Select random pattern as default, there can be no matches yet
        pattern_with_most_occurances = random.choice(list(self.patterns.keys()))

        # Find matching pattern with most occurances
        for window_match in window_matches:
            if pattern_with_most_occurances == None or self.patterns[window_match] > self.patterns[pattern_with_most_occurances]:
                pattern_with_most_occurances = window_match
            # Select randomly among two patterns with equal occurance count
            elif self.patterns[window_match] == self.patterns[pattern_with_most_occurances]:
                pattern_with_most_occurances = random.choice([window_match, pattern_with_most_occurances])

        # Return last element in pattern, this is next predicted event
        return pattern_with_most_occurances[-1]

    def add_event(self, event: any):

        # Remove old event, if max_events is reached
        if len(self.events) >= self.max_events:

            # Pattern should also be removed/it`s count should be decreased
            pattern_to_remove = tuple(self.events[0:self.N])
            assert pattern_to_remove in self.patterns.keys()

            self.patterns[pattern_to_remove] -= 1
            if self.patterns[pattern_to_remove] <= 0:
                del self.patterns[pattern_to_remove]
            del self.events[0]

        # Add new event to the queue
        self.events.append(event)

        # Create pattern if there are enough events/increment pattern
        if len(self.events) >= self.N:
            new_pattern = tuple(self.events[-self.N:])
            if new_pattern in self.patterns:
                self.patterns[new_pattern] += 1
            else:
                self.patterns[new_pattern] = 1

    events = []
    patterns = dict()


def _rps_():
    # Simple GUI using Tkinter
    import tkinter as tk

    rock = "Rock"
    paper = "Paper"
    scissors = "Scissors"

    draw = "Draw"
    win = "Win"
    loss = "Loss"

    # Let`s inherit from Tk, aka main window
    class RPS(tk.Tk):

        # Initialization

        def __init__(self, ngram: Ngram):
            super().__init__()

            self._create_move_buttons()

            self._create_outcome_label()
            self._create_round_label()
            self._create_score_label()

            self.ngram = ngram


        # GUI code

        def _create_move_buttons(self):
            create_move_button = lambda move: tk.Button(
                self, text=move, width=20, command=lambda: self.player_move(move)
            )

            self.rock_button = create_move_button(rock)
            self.rock_button.grid(row=0, column=0)

            self.paper_button = create_move_button(paper)
            self.paper_button.grid(row=1, column=0)

            self.scissors_button = create_move_button(scissors)
            self.scissors_button.grid(row=2, column=0)

        def _create_outcome_label(self):
            self.outcome_label = tk.Label(
                self, text="Ro-sham-bo!", width=20
            )
            self.outcome_label.grid(row=0, column=1)

        def _update_outcome_label(self, outcome: str):
            self.outcome_label.config(text=f"{outcome}!")

        def _create_round_label(self):
            self.round_label = tk.Label(
                self, text="Fight!", width=20
            )
            self.round_label.grid(row=1, column=1)

        def _update_round_label(self, player: str, ai: str):
            self.round_label.config(text=f"{player} vs {ai}")

        def _create_score_label(self):
            self.score_label = tk.Label(
                self, text="0 : 0", width=20
            )
            self.score_label.grid(row=2, column=1)

        def _update_score_label(self):
            self.score_label.config(text=f"{self.player_score} : {self.ai_score}")


        # Game logic

        def ai_move(self):
            # Next player move prediction
            # Use N-gram, if there is one
            player_next_possible_move = None
            if self.ngram != None:
                player_next_possible_move = self.ngram.predict_event()
            else:
                player_next_possible_move = random.choice([rock, paper, scissors])

            assert player_next_possible_move != None

            # Return countering move
            return self.get_counter_move(player_next_possible_move)

        def player_move(self, what: str):
            player_move = what
            ai_move = self.ai_move() # AI moves based on predicted next player move

            self._update_round_label(player_move, ai_move)

            # Check if player wins
            outcome = self.check_outcome(player_move, ai_move)

            self._update_outcome_label(outcome)

            if outcome == win:
                self.add_score(True) # Human player won
            elif outcome == loss:
                self.add_score(False) # AI won

            self._update_score_label()

            # Add new player move to the N-gram
            if self.ngram != None:
                ngram.add_event(player_move)

        def get_counter_move(self, move: str):
            counter = "???"

            if move == rock:
                counter = paper
            elif move == paper:
                counter = scissors
            elif move == scissors:
                counter = rock

            assert counter in [rock, paper, scissors], f"{counter}, {move}"

            return counter

        def check_outcome(self, player: str, ai: str):
            outcome = "???"

            if player == rock:
                if ai == rock:
                    outcome = draw
                elif ai == paper:
                    outcome = loss
                elif ai == scissors:
                    outcome = win
            elif player == paper:
                if ai == rock:
                    outcome = win
                elif ai == paper:
                    outcome = draw
                elif ai == scissors:
                    outcome = loss
            elif player == scissors:
                if ai == rock:
                    outcome = loss
                elif ai == paper:
                    outcome = win
                elif ai == scissors:
                    outcome = draw

            assert outcome in [win, loss, draw]

            return outcome

        player_score = 0
        ai_score = 0

        def add_score(self, player: bool):
            if player:
                self.player_score += 1
            else:
                self.ai_score += 1

    ngram = Ngram(4, [rock, paper, scissors], 1000)
    rps = RPS(ngram)

    # Start Tkinter loop
    return rps.mainloop()

if __name__ == '__main__':
    code = _rps_()

    exit(code)

