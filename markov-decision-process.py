# Markov Decision Process example

# Robot that searches for trash and collects it.
#  It has high and low power charge levels, can wait, search or go to recharge (defined as a 'state machine').
#  Robot receives some reward for actions. Goal - find optimal policy for this robot that yields maximum reward

def main():
    high, low = 'high', 'low'
    all_states = {high, low}

    wait, search, recharge = 'wait', 'search', 'recharge'
    states_prime = {(high, wait): {high}, (high, search): {low, high}, (low, wait): {low},
                    (low, search): {low, high}, (low, recharge): {high}}

    actions = {high: {wait, search}, low: {wait, search, recharge}}

    probabilities = {
        (high, wait, high): 1.0,
        (high, search, high): 0.6,
        (high, search, low): 0.4,
        (low, wait, low): 1.0,
        (low, search, high): 0.7,
        (low, search, low): 0.3,
        (low, recharge, high): 1.0
    }

    wait_reward = 1.0
    search_reward = 20.0
    discharged_reward = -3.0
    recharge_reward = 0.0

    rewards = {
        (high, wait, high): wait_reward,
        (high, search, high): search_reward,
        (high, search, low): search_reward,
        (low, wait, low): wait_reward,
        (low, search, high): discharged_reward,
        (low, search, low): search_reward,
        (low, recharge, high): recharge_reward
    }

    discount = 0.7

    def is_end(in_state: str):
        return False and in_state == 'end'

    state_optimal_values = {high: 0.0, low: 0.0}
    state_optimal_policies = {high: None, low: None}

    while True:
        state_optimal_values_old = state_optimal_values.copy()
        for state in all_states:
            if is_end(state):
                continue

            max_value = 0.0
            max_value_action = ''

            for action in actions[state]:
                state_action = (state, action)

                utility = 0.0
                for state_prime in states_prime[state_action]:
                    state_action_state_prime = state_action + (state_prime,)

                    # Bellman
                    utility += probabilities[state_action_state_prime] * \
                        (rewards[state_action_state_prime] + discount * state_optimal_values[state_prime])

                if utility > max_value:
                    max_value = utility
                    max_value_action = action

            state_optimal_values[state] = max_value
            state_optimal_policies[state] = max_value_action

        max_diff = 0.0
        for state in all_states:
            diff = abs(state_optimal_values[state] - state_optimal_values_old[state])
            if diff > max_diff:
                max_diff = diff
        if max_diff <= 0.000001:
            break

    print(state_optimal_values)
    print(state_optimal_policies)


if __name__ == '__main__':
    main()
